# README #

# Summary
A simple test configuration to test out the Feabhas STM32 board and WMS using the Segger J-Link and screen for console IO.

## Dependences
* J-LInk drivers installed, specifically JLinkExe
* J-Link connected to STM32 Board
* STM32 board and WMS powered

## Running
First setup a screen terminal using the default of 9600,8,n,1
```
$ screen /dev/<USB_Virtual_Serial_Port>
```
In another session invoke the shell script **download.sh**
```
$ ./download.sh 
SEGGER J-Link Commander V5.10s (Compiled Mar  9 2016 18:54:51)
DLL version V5.10s, compiled Mar  9 2016 18:54:45


Script file read successfully.
Processing script file...

J-Link connection not established yet but required for command.
Connecting to J-Link via USB...O.K.
Firmware: J-Link ARM V8 compiled Nov 28 2014 13:44:46
Hardware version: V8.00
S/N: 268000806
License(s): FlashBP, GDB
OEM: SEGGER-EDU
Emulator has Trace capability
VTref = 3.280V
Device "STM32F407VG" selected.
Found SWD-DP with ID 0x2BA01477
Found SWD-DP with ID 0x2BA01477
Found Cortex-M4 r0p1, Little endian.
FPUnit: 6 code (BP) slots and 2 literal slots
CoreSight components:
ROMTbl 0 @ E00FF000
ROMTbl 0 [0]: FFF0F000, CID: B105E00D, PID: 000BB00C SCS
ROMTbl 0 [1]: FFF02000, CID: B105E00D, PID: 003BB002 DWT
ROMTbl 0 [2]: FFF03000, CID: B105E00D, PID: 002BB003 FPB
ROMTbl 0 [3]: FFF01000, CID: B105E00D, PID: 003BB001 ITM
ROMTbl 0 [4]: FFF41000, CID: B105900D, PID: 000BB9A1 TPIU
ROMTbl 0 [5]: FFF42000, CID: B105900D, PID: 000BB925 ETM


TotalIRLen = 9, IRPrint = 0x0011
TotalIRLen = 9, IRPrint = 0x0011
Found Cortex-M4 r0p1, Little endian.
FPUnit: 6 code (BP) slots and 2 literal slots
CoreSight components:
ROMTbl 0 @ E00FF000
ROMTbl 0 [0]: FFF0F000, CID: B105E00D, PID: 000BB00C SCS
ROMTbl 0 [1]: FFF02000, CID: B105E00D, PID: 003BB002 DWT
ROMTbl 0 [2]: FFF03000, CID: B105E00D, PID: 002BB003 FPB
ROMTbl 0 [3]: FFF01000, CID: B105E00D, PID: 003BB001 ITM
ROMTbl 0 [4]: FFF41000, CID: B105900D, PID: 000BB9A1 TPIU
ROMTbl 0 [5]: FFF42000, CID: B105900D, PID: 000BB925 ETM
Found 2 JTAG devices, Total IRLen = 9:
 #0 Id: 0x4BA00477, IRLen: 04, IRPrint: 0x1, CoreSight JTAG-DP (ARM)
 #1 Id: 0x06413041, IRLen: 05, IRPrint: 0x1, STM32 Boundary Scan
Cortex-M4 identified.

Erasing device (STM32F407VG)...
Comparing flash   [100%] Done.
Erasing flash     [100%] Done.
Verifying flash   [100%] Done.
J-Link: Flash download: Total time needed: 17.393s (Prepare: 0.109s, Compare: 0.000s, Erase: 17.269s, Program: 0.000s, Verify: 0.000s, Restore: 0.014s)
Erasing done.

Downloading file [stm32_board_test.bin]...Comparing flash   [100%] Done.
Erasing flash     [100%] Done.
Programming flash [100%] Done.
Verifying flash   [100%] Done.
J-Link: Flash download: Flash programming performed for 1 range (32768 bytes)
J-Link: Flash download: Total time needed: 0.649s (Prepare: 0.151s, Compare: 0.005s, Erase: 0.000s, Program: 0.430s, Verify: 0.001s, Restore: 0.060s)
O.K.

Reset delay: 0 ms
Reset type NORMAL: Resets core & peripherals via SYSRESETREQ & VECTRESET bit.



Script processing completed.

$ 
```

Back in the screen terminal window you should now see:
```
Feabhas Limited
Command Line Interface
? for help

# 
```
Typing **?** will give you a list of options to test:
```
# ?

Available commands are:
--------------------------------------------------------
[d3 | d4 | d5 | d6]    [on | off]
leds                   [on | off | run]
motor                  [on | off | cd | run]
ss                     [[0..15] | blank | run]
buzzer                 [on | off]
latch                  [set | reset | clear]
read                   [all | door | ps1 | ps2 | ps3 | cancel | accept | disk
help | ?

# 
```

The current CLI supports last command repeat using **!!** and also support the use of backspace for editing.

## Testing the setup

To see if JLinkExe and the J-Link are working correctly simply type:
```
$ JLinkExe
SEGGER J-Link Commander V5.10s (Compiled Mar  9 2016 18:54:51)
DLL version V5.10s, compiled Mar  9 2016 18:54:45

Connecting to J-Link via USB...O.K.
Firmware: J-Link ARM V8 compiled Nov 28 2014 13:44:46
Hardware version: V8.00
S/N: 268000782
License(s): FlashBP, GDB
OEM: SEGGER-EDU
Emulator has Trace capability
VTref = 3.287V


Type "connect" to establish a target connection, '?' for help
J-Link>
```